Installation
#################
Here an outline of the different methods of installation available for VBAED. Much of the installation should be automated however the requirements should be checked as certain system architectures and operating sytems may require special versions of python modules


Standard Installation
*************************
For general operation of the program please use the following instructions

windows 10 64 bit
===================
.. todo:: docs unfinished

ubuntu 16.04 64 bit
===================
.. todo:: docs unfinished


developer install
******************
Here are the the ways to install the program for interactive development

windows 10 64 bit
===================
python3 -m pip install -e <dir of folder containing setup.py>





Online docs
============
online docs https://kreals.bitbucket.io/
